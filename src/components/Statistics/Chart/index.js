import React, {Component} from 'react'
import { withStyles } from '@material-ui/core/styles';

import {
    AreaChart, Area, Legend, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer
  } from 'recharts';


  const useStyles = theme => ({
    chart:  {
        width: '95%',
        height: '300'
      }
  });

class Chart extends Component  {
    constructor(props) {
        super(props)
        this.state = {
            data: this.props.data
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.data !== prevProps.data) {
            this.setState({data: this.props.data})
            return false
        } else {
            return true
        }
    }

    Chart = (data) => {
        const { classes } = this.props;
        return (
            <ResponsiveContainer width='98%' height={400}>
                <AreaChart
                    data={data}
                    margin={{
                    top: 30, right: 30, left: 0, bottom: 20,
                    }}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Area type="monotone" dataKey="Succ" stackId="1" stroke="#61b265" fill="#8bcd8e" />
                    <Area type="monotone" dataKey="Fail" stackId="1" stroke="#d55151" fill="#ff9393" />
                    <Area type="monotone" dataKey="Unknown" stackId="1" stroke="#8f8f8f" fill="#bebebe" />
                </AreaChart>
            </ResponsiveContainer>

        )
    }

	render() {
        const chartDate = this.state.data
        return (
            this.Chart(chartDate)
        );
	}

}

export default withStyles(useStyles)(Chart)
