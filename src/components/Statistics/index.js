import React, {Component} from 'react'
import Chart from './Chart'

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const testData = {'SSW-3.14': [
    {
        name: '3.14.2.14', Succ: 30, Fail: 20, Unknown: 40,
    },
    {
        name: '3.14.2.15', Succ: 25, Fail: 35, Unknown: 33,
    },
    {
        name: '3.14.2.16', Succ: 33, Fail: 44, Unknown: 38,
    },
    {
        name: '3.14.2.17', Succ: 66, Fail: 22, Unknown: 55,
    },
    {
        name: '3.14.2.18', Succ: 35, Fail: 93, Unknown: 22,
    },
    {
        name: '3.14.2.19', Succ: 61, Fail: 52, Unknown: 13,
    },
    {
        name: '3.14.2.20', Succ: 36, Fail: 30, Unknown: 29,
    },
  ],
  'SSW-3.15': [
    {
        name: '3.15.2.14', Succ: 1, Fail: 20, Unknown: 40,
    },
    {
        name: '3.15.2.15', Succ: 99, Fail: 35, Unknown: 33,
    },
    {
        name: '3.15.2.16', Succ: 99, Fail: 44, Unknown: 38,
    },
    {
        name: '3.15.2.17', Succ: 99, Fail: 22, Unknown: 55,
    },
    {
        name: '3.15.2.18', Succ: 99, Fail: 93, Unknown: 22,
    },
    {
        name: '3.14.2.19', Succ: 99, Fail: 52, Unknown: 13,
    },
    {
        name: '3.15.2.20', Succ: 99, Fail: 30, Unknown: 29,
    },
  ],
  'SMG': [
    {
        name: '2.15.2.14', Succ: 8, Fail: 5, Unknown: 1,
    },
    {
        name: '2.15.2.15', Succ: 8, Fail: 5, Unknown: 1,
    },
    {
        name: '2.15.2.16', Succ: 8, Fail: 5, Unknown: 1,
    },
    {
        name: '2.15.2.17', Succ: 8, Fail: 5, Unknown: 1,
    },
    {
        name: '2.15.2.18', Succ: 8, Fail: 5, Unknown: 1,
    },
    {
        name: '2.14.2.19', Succ: 8, Fail: 5, Unknown: 1,
    },
    {
        name: '2.15.2.20', Succ: 8, Fail: 5, Unknown: 1,
    },
  ]};

class Statistics extends Component  {
    constructor(props) {
        super(props)

        this.data = testData
        this.projectList = Object.keys(this.data)

        this.state = {
            project: this.projectList[0]
        }
    }

    useStyles = () => makeStyles(theme => ({
        root: {
          flexGrow: 1,
        },
        paper: {
          padding: theme.spacing(2),
          textAlign: 'center',
          color: theme.palette.text.secondary,
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
          },
        selectEmpty: {
        marginTop: theme.spacing(2),
        }
      }));


    handleChange = (event, newValue) => {
        this.setState({ project: this.projectList[newValue]})
    }

	render() {
        const classes = this.useStyles();
        const chartData = this.data[this.state.project];


        return (
            <div className={classes.root}>
                <Grid container spacing={3}>
                    <Grid item xs={12}>
                        <Tabs
                            value={this.projectList.indexOf(this.state.project)}
                            onChange={this.handleChange}
                            indicatorColor="primary"
                            textColor="primary"
                            centered
                            >
                            {this.projectList.map((res) => 
                                    <Tab key={this.projectList.indexOf(res)} label={res} />)}
                        </Tabs>
                    </Grid>
                    <Grid item xs={12}>
                        <Paper className={classes.paper}>
                            <Chart dimensions='1600' data={chartData}/>
                        </Paper>
                    </Grid>
                </Grid>
            </div>
        );
//        <Paper className={classes.paper}><Chart data={this.data[this.state.project]}/></Paper>

	}

}

export default Statistics