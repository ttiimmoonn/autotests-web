import React, {Component} from 'react'
import { Link } from 'react-router-dom'


import { makeStyles } from '@material-ui/core/styles';
import Collapse from '@material-ui/core/Collapse';
import Divider from '@material-ui/core/Divider';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';

import EqualizerIcon from '@material-ui/icons/Equalizer';
import NotesIcon from '@material-ui/icons/Notes';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import SettingsIcon from '@material-ui/icons/Settings';
import FeaturedPlayListIcon from '@material-ui/icons/FeaturedPlayList';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import AccountTreeIcon from '@material-ui/icons/AccountTree';


class MenuList extends Component  {
    constructor(props) {
        super(props)

        this.state = {
            selected: this.props.menuItem,
            redirect: false,
            openLaunchExtraMenu: false
        }
    }

    useStyles = () => makeStyles(theme => ({
        root: {
          width: '100%',
          maxWidth: 200,
          backgroundColor: theme.palette.background.paper,
        },
        nested: {
          paddingLeft: theme.spacing(4),
        },
      }));

    handleClickMenuItem = () => {

    };

    handleClickLaunchExtraMenu = () => {
        let state = this.state
        state.openLaunchExtraMenu = !this.state.openLaunchExtraMenu
        this.setState(state);
      };


    render() {
        const classes = this.useStyles()

        return (
                <List>
                    <ListItem button key={'Statistics'} component={ Link } to='/statistics' id='Statistics'>
                        <ListItemIcon> <EqualizerIcon/></ListItemIcon>
                        <ListItemText primary={'Statistics'} />
                    </ListItem>
                    <ListItem button key={'Launch'} component={ Link } to='/launch' id='Launch' >
                        <ListItemIcon> <PlayCircleOutlineIcon /></ListItemIcon>
                        <ListItemText primary={'Launch'} />
                        {this.state.openLaunchExtraMenu ? <ExpandLess onClick={this.handleClickLaunchExtraMenu} /> : <ExpandMore onClick={this.handleClickLaunchExtraMenu} />}
                    </ListItem>
                    
                    <Collapse in={this.state.openLaunchExtraMenu} timeout="auto" unmountOnExit>
                        <Divider />
                        <List component="div" disablePadding>
                            <ListItem button className={classes.nested} component={ Link } to='/launch/patterns' id='Patterns'>
                                <ListItemIcon> <FeaturedPlayListIcon /> </ListItemIcon>
                                <ListItemText primary={"Patterns"} />
                            </ListItem>
                            <ListItem button className={classes.nested} component={ Link } to='/launch/options' id='Options'>
                                <ListItemIcon> <SettingsIcon /> </ListItemIcon>
                                <ListItemText primary={"Options"} />
                            </ListItem>
                        </List>
                        <Divider />
                    </Collapse>

                    <ListItem button key={'Tests'} component={ Link } to='/tests' id='Tests'>
                        <ListItemIcon> <NotesIcon /></ListItemIcon>
                        <ListItemText primary={'Tests'} />
                    </ListItem>

                    <ListItem button key={'Projects'} component={ Link } to='/projects' id='Projects'>
                        <ListItemIcon> <AccountTreeIcon /></ListItemIcon>
                        <ListItemText primary={'Projects'} />
                    </ListItem>
                </List>
    )}
}

export default MenuList