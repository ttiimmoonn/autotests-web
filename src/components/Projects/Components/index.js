import React, {Component} from 'react'

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

import AddIcon from '@material-ui/icons/Add';
import Versions from '../Versions'
import ComponentCreateForm from '../CreateForm/componentCreateForm'
import ComponentDeleteForm from '../CreateForm/componentDeleteForm'
import ComponentEditForm from '../CreateForm/componentEditForm'


const useStyles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.primary,
    },
    paper_project: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.primary,
    },
    list_root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
      },  
    list_root_nested: {
        paddingLeft: theme.spacing(4),
      },
    button_add:  {
        marginTop: theme.spacing(2),
        bottom: theme.spacing(2),
        right: theme.spacing(2),
      },
      table: {
        minWidth: 650,
        width: '100%',
        marginTop : theme.spacing(3),
        overflowX: 'auto'
      },
  });

  class Components extends Component  {
    constructor(props) {
        super(props)
        this.api = props.api
        this.projectName = props.projectName

        
        this.state = {
            components: [],
            gettingContent: "false",
            openProjects: [],
            openEditForm: {
                state: false,
                projectName: '',
                componentName: '',
                componentDescription: ''
            },
            openDeleteForm: {
                state: false,
                projectName: '',
                componentName: ''
            }
        }
    }

    
    componentDidMount() {
        this.api.component_list({projectName: this.projectName})
        .then(this.api.checkStatus)
        .then((result) => {
            console.log(result)
            this.setState (
                {
                    components: result,
                    gettingContent: "succ"
                }
            )
        })
        .catch((error) => {
            console.log(error)
        })
    }

    updateComponent = () => {
        this.api.component_list({projectName: this.projectName})
        .then(this.api.checkStatus)
        .then((result) => {
            console.log(result)
            this.setState (
                {
                    components: result,
                    gettingContent: "succ"
                }
            )
        })
        .catch((error) => {
            console.log(error)
        })
    }

    handleClickShowProject = (projectName) => {
        let newProjects = this.state.openProjects
        if (newProjects.includes(projectName)) {
            delete newProjects[newProjects.indexOf(projectName)]
        } else {
            newProjects.push(projectName)
        }
        this.setState({
            openProjects: newProjects
        })
    }

    handleCreateFormOpen = () => {
        this.setState({
            openCreateForm: !this.state.openCreateForm
        })
    }

    handleClickEditComponent = (projectName, componentName, componentDescription) => {
        this.setState({
            openEditForm: {
                state: !this.state.openEditForm.state,
                projectName: projectName,
                componentName: componentName,
                componentDescription: componentDescription
            }
        })
    }

    handleClickDeleteComponent = (projectName, componentName) => {
        this.setState({
            openDeleteForm: {
                state: !this.state.openDeleteForm.state,
                projectName: projectName,
                componentName: componentName
            }
        })
    }

    updateProject = () => {
        this.api.project_list_all()
        .then(this.api.checkStatus)
        .then((result) => {
            console.log(result)
            this.setState (
                {
                    projectList: result,
                    gettingContent: "succ"
                }
            )
        })
        .catch((error) => {
            console.log(error)
        })
    }

	render() {
        const { classes } = this.props;
        return (
            <Grid item xs={12}>
                <Divider></Divider>
                <List component="nav" className={classes.list_root} aria-label="mailbox folders">
                    {this.state.components.map((comp) => 
                        <Grid>
                            <ListItem button key={comp._id} className={classes.list_root_nested} onClick={() => this.handleClickShowProject(this.projectName+comp.name)}>
                                <ListItemText primary={comp.name} secondary={comp.description} />
                            </ListItem>
                            <Collapse in={this.state.openProjects.includes(this.projectName+comp.name)} timeout="auto" unmountOnExit>
                                <Box  display="flex" justifyContent="flex-end">
                                    <ButtonGroup size="small" variant="text" color="large" aria-label="small text large button group">
                                        <Button onClick={() => this.handleClickEditComponent(this.projectName, comp.name, comp.description)}>Edit</Button>
                                        <Button onClick={() => this.handleClickDeleteComponent(this.projectName, comp.name)}>Delete</Button>
                                    </ButtonGroup>
                                </Box>
                                <Divider></Divider>
                                <Versions projectName={this.projectName} componentName={comp.name} api={this.api}/>
                            </Collapse>
                        </Grid>
                        )}
                </List> 
                <Box  display="flex" justifyContent="flex-end">
                    <Fab color="secondary" size="medium" className={classes.button_add} onClick={this.handleCreateFormOpen} aria-label="add" >
                        <AddIcon />
                    </Fab>
                </Box>
                <ComponentDeleteForm 
                    open={this.state.openDeleteForm} 
                    update={this.updateComponent} 
                    projectName={this.projectName} 
                    dialog='Component' 
                    close={this.handleClickDeleteComponent} 
                    api={this.api}
                />
                <ComponentEditForm 
                    open={this.state.openEditForm} 
                    update={this.updateComponent} 
                    projectName={this.projectName} 
                    dialog='Component' 
                    close={this.handleClickEditComponent} 
                    api={this.api}
                />
                <ComponentCreateForm 
                    open={this.state.openCreateForm} 
                    update={this.updateComponent} 
                    projectName={this.projectName} 
                    dialog='Component' 
                    close={this.handleCreateFormOpen} 
                    api={this.api}
                />
            </Grid>
        )
    }

}

export default withStyles(useStyles)(Components)