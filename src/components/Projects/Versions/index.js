import React, {Component} from 'react'

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableContainer from '@material-ui/core/TableContainer';


import AddIcon from '@material-ui/icons/Add';

const useStyles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.primary,
    },
    paper_project: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.primary,
    },
    list_root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
      },  
    list_root_nested: {
        paddingLeft: theme.spacing(4),
      },
    button_add:  {
        marginTop: theme.spacing(2),
        bottom: theme.spacing(2),
        right: theme.spacing(2),
      },
    table: {
        minWidth: 650,
        width: '100%',
        marginTop : theme.spacing(3),
        overflowX: 'auto'
    },
  });

  class Versions extends Component  {
    constructor(props) {
        super(props)
        this.api = props.api
        this.projectName = props.projectName
        this.componentName = props.componentName
        this.state = {
            versionList: [],
            gettingContent: "false",
            openProjects: []
        }
    }

    componentDidMount() {
        this.api.version_list({
            projectName: this.projectName, 
            componentName: this.componentName
        })
        .then(this.api.checkStatus)
        .then((result) => {
            this.setState (
                {
                    versionList: result,
                    gettingContent: "succ"
                }
            )
        })
        .catch((error) => {
            console.log(error)
        })
    }


	render() {
        const { classes } = this.props;
        return (
            <TableContainer component={Paper}>
            <Table className={classes.table}  size="small" aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Version</TableCell>
                  <TableCell align="right">Calories</TableCell>
                  <TableCell align="right">Fat&nbsp;(g)</TableCell>
                  <TableCell align="right">Carbs&nbsp;(g)</TableCell>
                  <TableCell align="right">Protein&nbsp;(g)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                  <TableRow key='1'>
                    <TableCell component="th" scope="row">
                        0.0.1
                    </TableCell>
                    <TableCell align="right">0.0.1</TableCell>
                    <TableCell align="right">222</TableCell>
                    <TableCell align="right">dsdsd</TableCell>
                    <TableCell align="right">1dw1</TableCell>
                  </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
            /*
            <Grid item xs={12}>
                <Paper className={classes.root}>    
                <Table aria-label="caption table">
                <caption>Table versions component {this.componentName}</caption>
                    <TableHead>
                    <TableRow>
                        <TableCell align="right">VSR</TableCell>
                        <TableCell align="right">Succ</TableCell>
                        <TableCell align="right">Fail</TableCell>
                        <TableCell align="right">Unknown</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {this.state.versionList.map((vrs) => (
                        <TableRow key={vrs.name}>
                        <TableCell component="th" scope="row">
                            {vrs.name}
                        </TableCell>
                        <TableCell align="right">{vrs.Succ === '' ? vrs.Succ : '-'}</TableCell>
                        <TableCell align="right">{vrs.Fail === '' ? vrs.Fail : '-'}</TableCell>
                        <TableCell align="right">{vrs.Unknown === '' ? vrs.Unknown : '-'}</TableCell>
                        </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </Paper>
            </Grid>
            */
        )
    }

}

export default withStyles(useStyles)(Versions)