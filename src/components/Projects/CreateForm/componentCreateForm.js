
import React, {Component} from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import Link from '@material-ui/core/Link';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';



const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class ComponentCreateForm extends Component {
    constructor(props) {
      super(props);
      this.projectName = props.projectName
      this.api = props.api
      this.update = props.update
      this.dialog = props.dialog
      this.close = props.close
      
      this.formData = {
        "name": "",
        "description": "",
        "userGroup": "",
        "projectName": ""
      }

      this.state = {
        open: props.open,
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.formData[event.target.name] = event.target.value
    }
  
    handleSubmit(event) {
      this.formData["userGroup"] = this.projectName
      this.formData['projectName'] = this.projectName
      this.api.component_create({'data': this.formData})
      .then(this.api.checkStatus)
      .then((result) => {
          console.log(result)
          this.close()
          this.update()
      })
      .catch((error) => {
          console.log(error)
      })
    }

    handleClose = (event) => {
      this.setState({
        open: false
      })
    }
  
    render() {
      const { classes } = this.props;

      console.log(this.state)
      return (
        <Dialog open={this.state.open} onClose={this.close} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Create new {this.dialog}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Create a new {this.dialog}. {this.dialog} names must always be unique. Do not forget to add a detailed description of the {this.dialog}.
            </DialogContentText>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="name"
                label="Component name"
                name="name"
                autoComplete="name"
                autoFocus
                onChange={this.handleChange}
              />
              <TextField
                margin="normal"
                required
                fullWidth
                multiline
                rows="2"
                variant="outlined"
                id="description"
                label="Description"
                name="description"
                autoComplete="description"
                autoFocus
                onChange={this.handleChange}
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.close} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Create
            </Button>
        </DialogActions>
        </Dialog>
      );
    }

    shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.open !== this.state.open) {
        this.setState({
          open: nextProps.open
        })
        return true
      } else {
        return false
      }
    }

  }

  export default withStyles(useStyles)(ComponentCreateForm)
