
import React, {Component} from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import Link from '@material-ui/core/Link';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';



const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class componentEditForm extends Component {
    constructor(props) {
      super(props);
      this.api = props.api
      this.dialog = props.dialog
      this.close = props.close
      this.update = props.update
      
      this.formData = {
        "name": "",
        "description": "",
        "userGroup": ""
      }

      this.state = {
        state: props.open.state,
        componentName: props.open.projectName,
        componentDescription: props.open.componentDescription
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.formData[event.target.name] = event.target.value
      console.log(this.formData)
    }
  
    handleSubmit(event) {
      this.api.component_update({
        'projectName': this.state.projectName, 
        'componentName': this.state.componentName, 
        'data': this.formData
      })
      .then(this.api.checkStatus)
      .then((result) => {
          console.log(result)
          this.close()
          this.update()
      })
      .catch((error) => {
          console.log(error)
      })
    }

    handleClose = (event) => {
      this.setState({
        open: false
      })
    }
  
    render() {
      const { classes } = this.props;

      console.log(this.state)
      return (
        <Dialog open={this.state.state} onClose={() => this.close(this.state.componentName)} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Update component</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Updating component settings.
            </DialogContentText>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                fullWidth
                id="name"
                label="Component name"
                name="name"
                autoComplete="name"
                defaultValue={this.state.componentName}
                onChange={this.handleChange}
              />
              <TextField
                margin="normal"
                fullWidth
                multiline
                rows="2"
                variant="outlined"
                id="description"
                label="Description"
                name="description"
                autoComplete="description"
                defaultValue={this.state.componentDescription}
                onChange={this.handleChange}
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.close(this.state.componentName)} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Update
            </Button>
        </DialogActions>
        </Dialog>
      );
    }

    shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.open.state !== this.state.state) {
        this.setState(nextProps.open)
        this.formData = {
          "name": nextProps.open.componentName,
          "description": nextProps.open.componentDescription,
          "userGroup": nextProps.open.projectName
        }
        return true
      } else {
        return false
      }
    }

  }

  export default withStyles(useStyles)(componentEditForm)
