
import React, {Component} from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';
import List from '@material-ui/core/List';
import Link from '@material-ui/core/Link';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';



const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class componentDeleteForm extends Component {
    constructor(props) {
      super(props);
      this.api = props.api
      this.close = props.close
      this.update = props.update
      
      this.formData = {
        "name": ""
      }

      this.state = {
        state: props.open.state,
        projectName: props.open.projectName,
        componentName: props.open.componentName,
      };

      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
  
    handleChange(event) {
      this.formData[event.target.name] = event.target.value
    }
  
    handleSubmit(event) {
      if (this.formData.name !== this.state.componentName) {
        this.formData.name = ''
        return false
      }
      this.api.component_delete({
        'projectName': this.state.projectName,
        'componentName': this.state.componentName
      })
      .then(this.api.checkStatus)
      .then((result) => {
          console.log(result)
          this.close()
          this.update()
      })
      .catch((error) => {
          console.log(error)
      })
    }

    handleClose = (event) => {
      this.setState({
        state: false
      })
    }
  
    render() {
      const { classes } = this.props;
      console.log(this.state)
      return (
        <Dialog open={this.state.state} onClose={() => this.close(this.state.componentName)} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Delete component {this.state.componentName}</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Delete component "{this.state.componentName}". When you delete a component, all version are also deleted. Enter component name to confirm deletion.
            </DialogContentText>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="name"
                label="Component name"
                name="name"
                autoComplete="name"
                autoFocus
                onChange={this.handleChange}
              />
            </form>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => this.close(this.state.componentDescription)} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Delete
            </Button>
        </DialogActions>
        </Dialog>
      );
    }

    shouldComponentUpdate(nextProps, nextState) {
      if (nextProps.open.state !== this.state.state) {
        this.setState(nextProps.open)
        return true
      } else {
        return false
      }
    }

  }

  export default withStyles(useStyles)(componentDeleteForm)
