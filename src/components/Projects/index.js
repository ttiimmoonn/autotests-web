import React, {Component} from 'react'

import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Fab from '@material-ui/core/Fab';
import Collapse from '@material-ui/core/Collapse';
import Box from '@material-ui/core/Box';


import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';

import AddIcon from '@material-ui/icons/Add';
import Components from './Components';
import ProjectCreateForm from './CreateForm/projectCreateForm';
import ProjectEditForm from './CreateForm/ProjectEditForm';
import ProjectDeleteForm from './CreateForm/ProjectDeleteForm';



const useStyles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.primary,
    },
    paper_project: {
      padding: theme.spacing(2),
      textAlign: 'center',
      color: theme.palette.text.primary,
    },
    list_root: {
        width: '100%',
        backgroundColor: theme.palette.background.paper,
      },  
    list_root_nested: {
        paddingLeft: theme.spacing(4),
      },
    project_control:  {
        marginTop: theme.spacing(2),
        bottom: theme.spacing(2),
        right: theme.spacing(2),
    },
        table: {
        minWidth: 650,
        width: '100%',
        marginTop : theme.spacing(3),
        overflowX: 'auto'
    }
  });


class Projects extends Component  {
    constructor(props) {
        super(props)
        this.projectList = []
        this.api = props.api

        this.state = {
            projectList: [],
            openProjects: [],
            gettingContent: "false",
            openCreateForm: false,
            openEditForm: {
                state: false,
                projectName: ''
            },
            openDeleteForm: {
                state: false,
                projectName: ''
            }
        }
    }

    componentDidMount() {
        this.api.project_list_all()
        .then(this.api.checkStatus)
        .then((result) => {
            console.log(result)
            this.setState (
                {
                    projectList: result,
                    gettingContent: "succ"
                }
            )
        })
        .catch((error) => {
            console.log(error)
        })
    }

    handleClickShowProject = (projectName) => {
        let newProjects = this.state.openProjects
        if (newProjects.includes(projectName)) {
            delete newProjects[newProjects.indexOf(projectName)]
        } else {
            newProjects.push(projectName)
        }
        this.setState({
            openProjects: newProjects
        })
    }

    handleClickEditProject = (projectName, description) => {
        this.setState({
            openEditForm: {
                state: !this.state.openEditForm.state,
                projectName: projectName,
                description: description
            }
        })
    }

    handleClickDeleteProject = (projectName) => {
        this.setState({
            openDeleteForm: {
                state: !this.state.openDeleteForm.state,
                projectName: projectName
            }
        })
    }

    updateProject = () => {
        this.api.project_list_all()
        .then(this.api.checkStatus)
        .then((result) => {
            console.log(result)
            this.setState (
                {
                    projectList: result,
                    gettingContent: "succ"
                }
            )
        })
        .catch((error) => {
            console.log(error)
        })
    }

    handleCreateFormOpen = () => {
        this.setState({
            openCreateForm: !this.state.openCreateForm
        })
    }

	render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                    <Paper elevation={2} className={classes.paper}>
                        <List component="nav" className={classes.list_root} aria-label="mailbox folders">
                            {this.state.projectList.map((project) => 
                                <Grid>
                                    <ListItem button key={project._id} onClick={() => this.handleClickShowProject(project.name)}>
                                        <ListItemText primary={project.name} secondary={project.description} />
                                    </ListItem>
                                    <Collapse in={this.state.openProjects.includes(project.name)} timeout="auto" unmountOnExit>
                                        <Box display="flex" justifyContent="flex-end">
                                            <ButtonGroup size="small" variant="text" color="large" aria-label="small text large button group">
                                                <Button onClick={() => this.handleClickEditProject(project.name, project.description)}>Edit</Button>
                                                <Button onClick={() => this.handleClickDeleteProject(project.name)}>Delete</Button>
                                            </ButtonGroup>
                                        </Box>
                                        <Components projectName={project.name} api={this.api}/>
                                    </Collapse>
                                    <Divider />
                                </Grid>
                                )}
                        </List> 
                        <Box  display="flex" justifyContent="flex-end">
                            <Fab color="primary" size="large" className={classes.button_add} onClick={this.handleCreateFormOpen} aria-label="add" >
                                <AddIcon />
                            </Fab>
                        </Box>
                    </Paper>
              </Grid>
              <Grid item xs={12}>
                <Paper className={classes.paper}>Api description</Paper>
              </Grid>
            </Grid>
            <ProjectCreateForm 
                open={this.state.openCreateForm} 
                dialog='Project' 
                update={this.updateProject} 
                close={this.handleCreateFormOpen} 
                api={this.api}
            />
            <ProjectEditForm 
                open={this.state.openEditForm} 
                dialog='Project' 
                update={this.updateProject} 
                close={this.handleClickEditProject} 
                api={this.api}
            />
            <ProjectDeleteForm open={this.state.openDeleteForm} dialog='Project' update={this.updateProject} close={this.handleClickDeleteProject} api={this.api}/>
          </div>
        );
	}

}

export default withStyles(useStyles)(Projects)