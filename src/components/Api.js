class Api {
    constructor(props) {
        this.httpV = props.httpV
        this.ip = props.ip
        this.port = props.port

        if (props.version) {
            this.version = props.version
        } else {
            this.version = 'v1'
        }

        if (props.headers) {
            this.headers = props.headers
        } else {
            this.headers = {}
        }

        this.mode = 'cors'

        
        this.serverUrl = `${this.httpV}://${this.ip}:${this.port}/api/${this.version}`
    }

    // Проекты
    // Список проектов с описанием и компонентами
    project_list_all = (props) => {
        let url = `${this.serverUrl}/project/list`
        console.log(url)
        let headers = this.headers
        let method = 'GET'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode
        })
    }    
    
    // Создать проект
    project_create = (props) => {
        let url = `${this.serverUrl}/project/create`
        console.log(url)
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props.data)
        })
    }    
    
    // Удалить проект
    project_delete = (props) => {
        let url = `${this.serverUrl}/project/${props.projectName}/delete`
        console.log(url)
        let headers = this.headers
        let method = 'DELETE'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode
        })
    }    
    
    // Обновление параметров проекта
    project_update = (props) => {
        let url = `${this.serverUrl}/project/${props.projectName}/set`
        console.log(url)
        let headers = this.headers
        let method = 'POST'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props.data)
        })
    } 

    // Компоненты
    // Получить весь список компонентов проекта
    component_list = (props) => {
        let url = `${this.serverUrl}/component/${props.projectName}/list`
        console.log(url)
        let headers = this.headers
        let method = 'GET'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode
        })
    }     
    
    // Создать компонент
    component_create = (props) => {
        let url = `${this.serverUrl}/component/${props.data.projectName}/create`
        console.log(url)
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props.data)
        })
    }      
    
    // Обновить компонент
    component_update = (props) => {
        let url = `${this.serverUrl}/component/${props.projectName}/${props.componentName}/set`
        console.log(url)
        let headers = this.headers
        headers['Content-Type'] = 'application/json;charset=utf-8'
        let method = 'POST'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode,
            body: JSON.stringify(props.data)
        })
    }     
    
    // Удалить компонент
    component_delete = (props) => {
        let url = `${this.serverUrl}/component/${props.projectName}/${props.componentName}/delete`
        console.log(url)
        let headers = this.headers
        let method = 'DELETE'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode
        })
    }    

    // Версии
    // Получить весь список версий проекта и компонента
    version_list = (props) => {
        let url = `${this.serverUrl}/version/${props.projectName}/${props.componentName}/list`
        console.log(url)
        let headers = this.headers
        let method = 'GET'
  
        return fetch(url, {
            method: method,
            credentials: this.credentials,
            headers: headers,
            mode: this.mode
        })
    } 


    checkStatus = (response) => {
        console.log(response)
        const contentType = response.headers.get("content-type");
        if (response.status >= 200 && response.status < 300) {
            if (contentType && contentType.indexOf("application/json") !== -1) {
                console.log('Есть тело и успех')
                return response.json();
            } else if (contentType && contentType.indexOf("text/plain") !== -1) {
                console.log('Есть тело и успех, ')
                return response.text();
            } else {
                console.log('Нет тела и успех')
                return;
            }
        } else {
            if (contentType && contentType.indexOf("application/json") !== -1) {
                console.log('Есть тело и fail | снаруже')
                return response.json()
                    .then((error) => {
                        console.log('Есть тело и fail | внутри', error)
                        throw new Error(error.message)
                    })
            } else {
                console.log('Нет тела и fail')
                throw new Error({ message: '' });
            }
        }
    }
}

export default Api