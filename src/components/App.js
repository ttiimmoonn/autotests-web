import React from 'react'
import Api from './Api'
import MiniDrawer from './Drawer'

function App() {
  let api = new Api({httpV: 'http', ip: '192.168.1.33', port: '8082'})
  console.log(api)
  return (
    <MiniDrawer api = {api}/>
    );
  }

export default App