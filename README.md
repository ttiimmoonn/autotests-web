## Используемые библиотеки

Material-UI
    Описание:
        Основные компоненты.
    Ресурс:
        https://material-ui.com
    Установка:
        npm install @material-ui/core
    
    Типография:
        Описание:
            Шрифты.
        Установка:
            npm install typeface-roboto --save
    
    Иконки:
        Описание:
            Набор иконок.
        Ресурс:
            https://material.io/resources/icons/?style=baseline
        Установка:
            sudo npm install @material-ui/icons

React-Router
    Описание:
        Маршрутизация на страницах.
    Установка:
        npm install react-router-dom

recharts
    Описание:
        Графики.
    Ресурс:
        http://recharts.org/